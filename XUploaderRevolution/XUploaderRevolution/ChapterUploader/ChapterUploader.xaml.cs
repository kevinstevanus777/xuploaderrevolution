﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace XUploaderRevolution
{
    
    /// <summary>
    /// Interaction logic for ChapterUploader.xaml
    /// </summary>
    public partial class ChapterUploader : Window
    {
        //source list
        List<String> sourcelist = new List<String>();
        List<String> errorlist = new List<String>();
        List<String> completelist = new List<String>();

        //public string from amazonform
        string amazonurl, amazonbucket, amazonmanganame, amazonregion;

        public ChapterUploader()
        {
            InitializeComponent();
        }

        private void RbAmazon_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            AmazonSourceForm form = new AmazonSourceForm();
            form.ShowDialog();

            amazonurl = form.TbAmazonURL.Text;
            amazonbucket = form.TBBucket.Text;
            amazonmanganame = form.TBMangaName.Text;
            amazonregion = form.TBRegion.Text;

        }

        private void BtRunChapterUploader_Click(object sender, RoutedEventArgs e)
        {
            

            //open chrome
            IWebDriver driver = new ChromeDriver();
            

            System.Threading.Thread.Sleep(2000);


            LoginToWordPress(driver);

            

            //looping per chapter line
            foreach(var sl in sourcelist)
            {

                string amazondetailedurl = amazonurl + amazonbucket + amazonmanganame + sl + amazonregion;
                //string replace = amazondetailedurl.Replace("\n", string.Empty);


                //MessageBox.Show(replace);


                //always change to manga the same manga link from textbox
                GoToMangaLink(driver);

                //click upload single chapter
                ClickUploadSingleChapter(driver);

                //modified string "_" become " "
                string temp = sl.Replace("_"," ");

                //set chapter title to clipboard
                SetTextToClipboard(temp);

                Console.WriteLine(temp);



                //paste chapter name to box
                SendToChapterNameBox(driver, temp);

                //Click "Select Uploaded Album From Cloud Server" Radio Button
                driver.FindElement(By.Id("select-album")).Click();

                //Console.WriteLine(amazondetailedurl);
                //set clipboard to amazon bucket link
                SetTextToClipboard(amazondetailedurl);
                //Console.WriteLine(amazondetailedurl);

                //search amazon album
                AmazonSearchAlbum(driver, amazondetailedurl);


                //click create chapter
                WpCreateChapter(driver, sl);

               



            }


            //from amazon
            if (RbAmazon.IsChecked == true)
            {



            }
        }

        private void RbAmazon_Checked(object sender, RoutedEventArgs e)
        {

        }

        
        private void Button_Click(object sender, RoutedEventArgs e)
        {


        }
        

        private void BtFetchFromClipBoard_Click(object sender, RoutedEventArgs e)
        {
            FetchFromClipboardForm form = new FetchFromClipboardForm();
            form.ShowDialog();

            //thisshouldbethelatest
            
            //if save button clicked
            if(form.saveclicked == true)
            {

                //https://stackoverflow.com/questions/22865444/how-to-get-all-text-from-richtextbox-in-wpf
                //throw richtexbox text to string "rich text"
                //string richtext = new TextRange(form.RTBSource.Document.ContentStart, form.RTBSource.Document.ContentEnd).Text; //old one

                TextRange newtextrange = new TextRange(form.RTBSource.Document.ContentStart, form.RTBSource.Document.ContentEnd);
                string richtext = newtextrange.Text.Replace(Environment.NewLine,"\n");

                //old one
                //string richtext = newtextrange.Text;

                

                //split the text to lines
                var lines = richtext.Split('\n').ToList();
                //int tmp = 0;
                //looping the var 
                foreach(string line in lines)
                {
                    


                    sourcelist.Add(line);
                   // Console.WriteLine(tmp+line+tmp);



                   // tmp++;


                    
                }



            }

            

            


        }

        private void BtSourceList_Click(object sender, RoutedEventArgs e)
        {
            TemporaryListForm form = new TemporaryListForm();
            form.RTBTemporaryList.Document.Blocks.Clear();
            
            foreach (string s in sourcelist)
            {
                form.RTBTemporaryList.Document.Blocks.Add(new Paragraph(new Run(s)));
                
            }
            form.ShowDialog();
            

            
        }

        private void RbAmazon_Checked_1(object sender, RoutedEventArgs e)
        {

        }



        public void LoginToWordPress(IWebDriver driver)
        {
            //go to wp url
            driver.Url = Properties.Settings.Default.WpLink;

            //fill username to cell
            driver.FindElement(By.Id("user_login")).SendKeys(Properties.Settings.Default.WpUsername);

            //fill password to cell
            driver.FindElement(By.Id("user_pass")).SendKeys(Properties.Settings.Default.WpPassword);

            //click login
            driver.FindElement(By.Id("wp-submit")).Click();


        }

        public void GoToMangaLink(IWebDriver driver)
        {
            try
            {
                driver.Url = TbWpMangaLink.Text;
                //uncomment pls
                //System.Threading.Thread.Sleep(delay);
            }
            catch (WebDriverTimeoutException)
            {


                try
                {

                    driver.Url = TbWpMangaLink.Text;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
                catch (WebDriverException)
                {

                    driver.Url = TbWpMangaLink.Text;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
            }

        }


        public void ClickUploadSingleChapter(IWebDriver driver)
        {
            //click "Upload Single Chapter" 

            //kykny perlu klik di bagian a href nya
            //disini ada masalah webdriverexception tadinya

            try
            {

                driver.FindElement(By.ClassName("manga-tab-select tab-active")).Click();
            }

            catch (NoSuchElementException)
            {
                try
                {
                    driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[2]/div/div[1]/div[1]/ul/li[3]/a")).Click();


                }

                catch (NoSuchElementException)
                {

                    try
                    {
                        driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/ul/li[3]/a")).Click();

                    }
                    catch (NoSuchElementException)
                    {
                        driver.FindElement(By.ClassName("manga-tab-select")).Click();

                    }

                }

            }
        }


        public void SetTextToClipboard(string input)
        {
            try
            {
                Clipboard.SetText(input);
            }
            catch (ArgumentNullException)
            {
                Clipboard.SetText(" ");
            }

        }

        private void BtCompleteList_Click(object sender, RoutedEventArgs e)
        {

        }

        public void SendToChapterNameBox(IWebDriver driver,string input)
        {
            //Send array to "Chapter Name Box"

            //OpenQA.Selenium.ElementNotVisibleException
            try
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ElementNotVisibleException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (NoSuchElementException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(input);
            }
        }


        public void AmazonSearchAlbum(IWebDriver driver,string input)
        {
            //Send array to "Search by Album Name"
            //driver.FindElement(By.Name("blogspot-album-name")).SendKeys(sl);
            try
            {
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ArgumentNullException)
            {
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(input);
            }


          
        }

        public void WpCreateChapter(IWebDriver driver,string input)
        {

            //kalo texbox yg ijo ato merah muncul... masukin ke stringlist(eror or complete)
            if (driver.FindElement(By.Id("chapter-upload-msg")).Displayed && driver.FindElement(By.Id("chapter-upload-msg")).Text == "Cannot find this album")
            {
                //MessageBox.Show("cannot find this album true");
                errorlist.Add(input);
            }
            else
            {
                //click on create chapter
                driver.FindElement(By.Id("import-album")).Click();
                //add to stringlist
                completelist.Add(input);
            }

        }

    }
}
