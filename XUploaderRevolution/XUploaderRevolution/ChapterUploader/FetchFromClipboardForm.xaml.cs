﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XUploaderRevolution
{
    /// <summary>
    /// Interaction logic for FetchFromClipboardForm.xaml
    /// </summary>
    public partial class FetchFromClipboardForm : Window
    {

        public Boolean saveclicked = false;
       
        public FetchFromClipboardForm()
        {
            InitializeComponent();
            
            
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {
            saveclicked = true;

            this.Close();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void FetchFromClipboard_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void RTBSource_TextChanged(object sender, TextChangedEventArgs e)
        {
            //refrence
            //https://stackoverflow.com/questions/325075/how-do-i-change-richtextbox-paragraph-spacing

        }
    }
}
