﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XUploaderRevolution
{
    /// <summary>
    /// Interaction logic for AmazonSourceForm.xaml
    /// </summary>
    public partial class AmazonSourceForm : Window
    {
        public AmazonSourceForm()
        {
            InitializeComponent();
        }

        private void BtCheck_Click(object sender, RoutedEventArgs e)
        {

           
            TbURLExample.Text = TbAmazonURL.Text + TBBucket.Text + TBMangaName.Text + "dynamic chapter" + TBRegion.Text;
        }
    }
}
