﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XUploaderRevolution
{
    /// <summary>
    /// Interaction logic for SettingsForm.xaml
    /// </summary>
    public partial class SettingsForm : Window
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void BtSave_Click(object sender, RoutedEventArgs e)
        {

            //get wordpress link to properties
            Properties.Settings.Default.WpLink = TbURL.Text;

            //get user name to properties
            Properties.Settings.Default.WpUsername = TbUsername.Text;

            //get password to properties
            Properties.Settings.Default.WpPassword = PbPassword.Password;
            
            //save properties
            Properties.Settings.Default.Save();

            //notification
            MessageBox.Show("saved");

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //load settings
            TbURL.Text = Properties.Settings.Default.WpLink;
            TbUsername.Text = Properties.Settings.Default.WpUsername;
            PbPassword.Password = Properties.Settings.Default.WpPassword;
            
        }

        private void BtCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
