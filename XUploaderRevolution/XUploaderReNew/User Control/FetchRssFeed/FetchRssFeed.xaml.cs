﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CodeHollow.FeedReader;
using System.IO;
using System.Net;


namespace XUploaderReNew.User_Control.FetchRssFeed
{
    /// <summary>
    /// Interaction logic for FetchRssFeed.xaml
    /// </summary>
    public partial class FetchRssFeed : UserControl
    {
        public FetchRssFeed()
        {
            InitializeComponent();
        }

        private void BtSubmit_Click(object sender, RoutedEventArgs e)
        {
            //string url = "https://mangadex.org/rss/sdxk3QcVwCGSZPqfX2N9Bt8UgDvRApyn";
            var feed = FeedReader.Read("https://mangadex.org/rss/sdxk3QcVwCGSZPqfX2N9Bt8UgDvRApyn");
            Console.WriteLine("Feed Title: " + feed.Title);
            Console.WriteLine("Feed Description: " + feed.Description);
            Console.WriteLine("Feed Image: " + feed.ImageUrl);
            // ...
            foreach (var item in feed.Items)
            {
                Console.WriteLine(item.Title);
                Console.WriteLine(item.Link);
                Console.WriteLine(item.Description);
                Console.WriteLine(item.PublishingDate);
                Console.WriteLine(item.Id);
                Console.WriteLine(item.Categories);
                Console.WriteLine(item.Author);
                Console.WriteLine(item.Content);
                Console.WriteLine(item.SpecificItem);
                Console.WriteLine();

                //Console.WriteLine(item.Title + " - " + item.Link + " - " + item.Description + '\n');
            }

        }

        private void BtNotification_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtHtmlAgilityPack_Click(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();

            // Add a user agent header in case the 
            // requested URI contains a query.

            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            string test = "https://www.mangaflame.com/wp-admin";
            Stream data = client.OpenRead(test);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            Console.WriteLine(s);
            data.Close();
            reader.Close();

        }
    }
}
