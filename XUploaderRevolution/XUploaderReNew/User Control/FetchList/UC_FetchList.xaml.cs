﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;



namespace XUploaderReNew.User_Control
{
    /// <summary>
    /// Interaction logic for UC_FetchListMangadex.xaml
    /// </summary>
    public partial class UC_FetchListMangadex : UserControl
    {

        IWebDriver driver;
        List<String> title = new List<string>(); //list of title
        List<int> titleid = new List<int>(); //list of title id
        List<String> titlelink = new List<string>(); //list of title link
        List<String> errorlink = new List<string>(); //list of error link
        List<String> totalrating = new List<string>(); //list of total rating
        List<String> totalviews = new List<string>(); //total views
        List<String> yearrelease = new List<string>(); //year release
        List<String> lastupdatestatus = new List<string>(); //last update
        List<String> mangatype = new List<string>();
        Boolean isbanned = false;



        public UC_FetchListMangadex()
        {
            InitializeComponent();
        }

        private void BtRun_Click(object sender, RoutedEventArgs e)
        {
            //reset list
            title = new List<string>(); //list of title
            titleid = new List<int>(); //list of title id
            titlelink = new List<string>(); //list of title link
            errorlink = new List<string>(); //list of error link
            totalrating = new List<string>(); //list of total rating
            totalviews = new List<string>(); //total views
            yearrelease = new List<string>(); //year release

            lastupdatestatus = new List<string>();
            

            //mangaupdates
            //myanimelist.net/manga/1




            //Boolean isbanned = false;                   //ban check
            int start = Int32.Parse(TbStart.Text);      //starting number
            int finish = Int32.Parse(TbFinish.Text);    //ending number
            string source = TbSource.Text;              //link source
            int delay = Int32.Parse(TbDelay.Text);      //delay 
            Boolean isheadless = false;                 //no browser
            Boolean skipimage = false;                  //skip image download
            string websitesource = "";

            //check if checkbox is checked for headless
            if (CbIsHeadless.IsChecked == true){isheadless = true;}

            //check if checkbox is checkec for image skip
            if(CBSkipImage.IsChecked == true){skipimage = true;}

            float elapsed_time;
            int mangadone = 0;
            int mangaleft = Int32.Parse(TbFinish.Text) - Int32.Parse(TbStart.Text) +1;


            //add chrome options
            ChromeOptions option = new ChromeOptions();


            if (isheadless == true) {option.AddArgument("--headless");}



            if (skipimage == true)
            {
                option.AddArgument("--blink-settings=imagesEnabled=false");
            }

            


            if(RbMangaUpdate.IsChecked == true)
            {
                websitesource = "mangaupdate";
            }
            else if(RbMangadex.IsChecked == true)
            {
                websitesource = "mangadex";
            }
            else if(RbMyAnimelist.IsChecked == true)
            {
                websitesource = "myanimelist";
            }


            Task TaskA = new Task(() =>
            {

                //buka chrome
                //IWebDriver driver = new ChromeDriver(option);
                driver = new ChromeDriver(option);

                for (int i = start; i <= finish; i++)
                {
                    Console.WriteLine(websitesource);
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    driver.Url = source + i;

                    //string isbannedstring = driver.FindElement(By.CssSelector("body")).Text;

                    //check ip ban

                    Checkipban(driver,websitesource);
                    if(isbanned == true)
                    {
                        break;
                    }


                    GetMangaTitle(driver, i,websitesource);

                    GetMangaRating(driver, i,websitesource);

                    GetMangaViews(driver, i,websitesource);


                    //get title id to list
                    titleid.Add(i);

                    //get manga link
                    titlelink.Add(driver.Url);


                    GetReleaseDate(driver, i, websitesource);

                    GetMangaType(driver, i, websitesource);

                    GetLastUpdateStatus(driver, i, websitesource);





                    System.Threading.Thread.Sleep(delay);

                    stopwatch.Stop();

                    elapsed_time = stopwatch.ElapsedMilliseconds;

                    mangadone++;
                    mangaleft--;

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LbSpeed.Content = elapsed_time + " ms"));


                    //update Manga Done in GUI
                    //https://stackoverflow.com/questions/1644079/change-wpf-controls-from-a-non-main-thread-using-dispatcher-invoke
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LbMangaDone.Content = mangadone + " Manga done"));

                    //update manga left in GUI 
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LbMangaLeft.Content = mangaleft + " Manga left"));



                }




            });
            TaskA.Start();

            /*
            if (something?)
            {
                LbProgramStatus.Content = "PROGRAM STOPPED";
            }*/
            

        }



        private void BtCheckList_Click(object sender, RoutedEventArgs e)
        {
            

            
        }

        public void Checkipban(IWebDriver driver,string websitesource)
        {
            if(websitesource == "mangadex")
            {
                if (driver.FindElement(By.CssSelector("body")).Text.Contains("Too many hits detected"))
                {
                    //isbanned = true;


                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LbBanned.Content = "YOU ARE BANNED"));
                    isbanned = true;
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => LbBanned.Content = "STILL NOT BANNED"));

                }
            }

            else if(websitesource == "mangaupdate")
            {

            }
        }

        public void GetLastUpdateStatus(IWebDriver driver, int i , string websitesource)
        {
            if(websitesource == "mangadex")
            {

            }
            else if(websitesource == "mangaupdate")
            {
                try
                {
                    lastupdatestatus.Add(driver.FindElement(By.CssSelector(".sContainer:nth-child(3) .sContent:nth-child(38)")).Text);
                }
                catch (NoSuchElementException)
                {
                    lastupdatestatus.Add("-");

                }
            }
        }

        public void GetReleaseDate(IWebDriver driver, int i, string websitesource)
        {
            if(websitesource == "mangadex")
            {

            }
            else if(websitesource == "mangaupdate")
            {
                try
                {
                    //yearrelease.Add(driver.FindElement(By.CssSelector(".sContainer:nth-child(4) .sContent:nth-child(23)")).Text);
                    yearrelease.Add(driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[4]/div[16]")).Text);

                }
                catch (NoSuchElementException)
                {
                    yearrelease.Add("-");

                }
            }
        }


        public void GetMangaTitle(IWebDriver driver, int i,string websitesource)
        {
            //get manga title to list
            if(websitesource == "mangadex")
            {
                try
                {



                    //title.Add(driver.FindElement(By.XPath("/html/body/div[1]/div[2]/h6")).Text);
                    title.Add(driver.FindElement(By.CssSelector("h6.card-header")).Text);
                }
                catch (NoSuchElementException)
                {
                    title.Add("MANGA DOES NOT EXIST OR DELETED");
                    Console.WriteLine("not found: " + i); //output to console the title id
                    errorlink.Add(driver.Url);
                }

            }
            else if(websitesource == "mangaupdate")
            {
                try
                {
                    ////meta[@property='og:description']
                    //title.Add(driver.FindElement(By.XPath("//span[@class='releasestitle tabletitle']")).Text);
                    //title.Add(driver.FindElement(By.CssSelector(".releasestitle")).Text);
                    title.Add(driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/span[1]")).Text);
                }
                catch (NoSuchElementException)
                {
                    title.Add("MANGA DOES NOT EXIST OR DELETED");
                    Console.WriteLine("not found: " + i); //output to console the title id
                    errorlink.Add(driver.Url);

                }
                    

            }


        }

        public void GetMangaType(IWebDriver driver, int i , string websitesource)
        {
            if(websitesource == "mangadex")
            {

            }
            else if(websitesource == "mangaupdate")
            {
                try
                {
                    //mangatype.Add(driver.FindElement(By.CssSelector(".sContainer:nth-child(3) .sContent:nth-child(5)")).Text);
                    mangatype.Add(driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[3]/div[4]")).Text);
                }
                catch (NoSuchElementException)
                {
                    mangatype.Add("-");
                }
            }
        }

        public void GetMangaRating(IWebDriver driver, int i,string websitesource)
        {
            if(websitesource == "mangadex")
            {
                try
                {
                    //rating
                    totalrating.Add(driver.FindElement(By.CssSelector("span.text-primary")).Text);

                }
                catch (NoSuchElementException)
                {
                    totalrating.Add("-");


                }
            }

            else if (websitesource == "mangaupdate")
            {

            }

        }


        public void GetMangaViews(IWebDriver driver, int i,string websitesource)

        {
            if(websitesource == "mangadex")
            {
                try
                {
                    //views
                    totalviews.Add(driver.FindElement(By.CssSelector("li.list-inline-item.text-info")).Text);
                }
                catch (NoSuchElementException)
                {
                    totalviews.Add("-");
                }

            }

            else if (websitesource == "mangaupdate")
            {

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtTitleListToClipboard_Click(object sender, RoutedEventArgs e)
        {

            DataToClipboard(title);
        }

        private void BtTitleIdToClipboard_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";

            foreach (int s in titleid)
            {
                if (temp == "")
                {
                    temp = s.ToString();
                }
                else
                {
                    temp = temp + '\n' + s.ToString();
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }
            

        }

        private void BtMangaLinkListToClipboard_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";

            foreach (string s in titlelink)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }
        }

        private void BtViewsListToClipboard_Click(object sender, RoutedEventArgs e)
        {

            DataToClipboard(totalviews);
        }

        private void BtRatingListToClipboard_Click(object sender, RoutedEventArgs e)
        {

            DataToClipboard(totalrating);
        }

        private void BtForceStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                driver.Close();
                driver.Quit();
                driver.Dispose();

            }
            catch (System.NullReferenceException)
            {

            }
            
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // If modifying these scopes, delete your previously saved credentials
            // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
            string[] Scopes = { SheetsService.Scope.Spreadsheets }; // static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
             string ApplicationName = "MangaflameAutoUpdater";
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);


                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });



            String spreadsheetId2 = "15YArGtKqNCE75zE3PAN6BARaRU5c3hKrrQ01ouC-4Gk";
            String range2 = "Mangaupdate!H5";  // update cell F5 
            ValueRange valueRange = new ValueRange();
            valueRange.MajorDimension = "COLUMNS";//"ROWS"   ;//COLUMNS

            var oblist = new List<object>() { "this is bot" };
            valueRange.Values = new List<IList<object>> { oblist };

            SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId2, range2);
            update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            UpdateValuesResponse result2 = update.Execute();

            Console.WriteLine("done!");
        }

        private void BtYearRelease_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";

            foreach (string s in yearrelease)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }
        }


        private void BtLastUpdate_Click(object sender, RoutedEventArgs e)
        {

            DataToClipboard(lastupdatestatus);

        }

        private void BtType_Click(object sender, RoutedEventArgs e)
        {
            DataToClipboard(mangatype);

        }


        public void DataToClipboard(List<String> input)
        {
            string temp = "";

            foreach (string s in input)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }
        }
    }
}

/*4:26 PM] Inzanit: probably store it to a field or variable before, rather than accessing TbStart.Text directly
[4:26 PM] Inzanit: other than that, I'd ask why you're creating a new Thread manually
[4:27 PM] Lacrimose: still had improper experience with threading :yum:
[4:27 PM] Lacrimose: this way im also learning threading
[4:27 PM] Lacrimose: bad implementation huh ?
[4:27 PM] Inzanit: var start = int.Parse(TbStart.Text); outside the new Thread(() => and then use start
[4:28 PM] Inzanit: you might be better of using tasks
[4:28 PM] Inzanit: https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-based-asynchronous-programming
Task-based Asynchronous Programming

[4:28 PM] Lacrimose: alright
[4:28 PM] Lacrimose: thank you for the help
[4:28 PM] Lacrimose: :smiley:
[4:29 PM] Lacrimose: whoa, its' working(by store it to a variable) . but it's still bad implementation..

*/


//https://stackoverflow.com/questions/37462887/update-a-cell-with-c-sharp-and-sheets-api-v4
//https://medium.com/@williamchislett/writing-to-google-sheets-api-using-net-and-a-services-account-91ee7e4a291