﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using System.Diagnostics;



namespace XUploaderReNew.User_Control.ChapterUploader
{
    /// <summary>
    /// Interaction logic for UC_MultiChapterUploader.xaml
    /// </summary>
    public partial class UC_MultiChapterUploader : UserControl
    {

        /// <summary>
        /// global variables
        /// </summary>
        List<String> sourcelist = new List<String>(); //list manga url


        public UC_MultiChapterUploader()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //INI CARA BIKIN 1 FORM BISA AKSES TOMBOL DI FORM LAIN. GANTI MODIFIERNYA JADI PUBLIC DULU DI FORM YANG DITUJU

            //UC_ChapterUploader f = new UC_ChapterUploader();
            //f.BtRunChapterUploader_Click(null,null);
            //f.TbDirectory.Text = "test";
            //f.BtFetchFromDirectory_Click(null, null);
            var list = new List<Tuple<string, string,string>>();
            list.Add(new Tuple<string, string, string>("adit", "jamal", "abidin"));
            list.Add(new Tuple<string, string, string>("hellyeah", "testingboi", "wth")); 


            Console.WriteLine(list[0].Item1);
            Console.WriteLine(list[0].Item2);
            Console.WriteLine(list[0].Item3);

            Console.WriteLine(list[1].Item3);


        }


        /// <summary>
        /// klik di bagian "Upload Single Chapter" , masih suka ngebug gara2 ya tau lah..
        /// </summary>
        /// <param name="driver"></param>
        public void ClickUploadSingleChapter(IWebDriver driver)
        {
            //click "Upload Single Chapter" 

            //kykny perlu klik di bagian a href nya
            //disini ada masalah webdriverexception tadinya

            try
            {

                driver.FindElement(By.ClassName("manga-tab-select tab-active")).Click();
            }

            catch (NoSuchElementException)
            {
                try
                {
                    driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[2]/div/div[1]/div[1]/ul/li[3]/a")).Click();


                }

                catch (NoSuchElementException)
                {

                    try
                    {
                        driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/ul/li[3]/a")).Click();

                    }
                    catch (NoSuchElementException)
                    {
                        driver.FindElement(By.ClassName("manga-tab-select")).Click();

                    }

                }

            }
        }

        /// <summary>
        /// melakukan klik ke link manga wordpress
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="WpMangaLink"></param>
        public void GoToMangaLink(IWebDriver driver, string WpMangaLink)
        {
            try
            {
                driver.Url = WpMangaLink;
                //uncomment pls
                //System.Threading.Thread.Sleep(delay);
            }
            catch (WebDriverTimeoutException)
            {


                try
                {

                    driver.Url = WpMangaLink;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
                catch (WebDriverException)
                {

                    driver.Url = WpMangaLink;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
            }

        }


        /// <summary>
        /// Get data from directories;
        /// </summary>
        private void FetchFromDirectory(string directorylink)
        {
            //kosongin
            sourcelist = new List<string>();

            //path
            string path = directorylink;


            DirectoryInfo dir = new DirectoryInfo(path);

            foreach (string s in Directory.GetDirectories(path))
            {
                sourcelist.Add((s.Remove(0, path.Length + 1)));
            }

            
        }


        /// <summary>
        /// WP Login
        /// </summary>
        /// <param name="driver"></param>
        public void LoginToWordPress(IWebDriver driver)
        {
            //go to wp url
            driver.Url = Properties.Settings.Default.WpLink;

            System.Threading.Thread.Sleep(2000);

            //fill username to cell
            driver.FindElement(By.Id("user_login")).SendKeys(Properties.Settings.Default.WpUsername);

            //fill password to cell
            driver.FindElement(By.Id("user_pass")).SendKeys(Properties.Settings.Default.WpPassword);

            //click login
            driver.FindElement(By.Id("wp-submit")).Click();


        }


        /// <summary>
        /// Untuk Input File CSV
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtBrowseDirectory_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            //https://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c-sharp/10315283
            //https://stackoverflow.com/questions/5282999/reading-csv-file-and-storing-values-into-an-array
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".csv";
            dlg.Filter = "CSV Files(*.csv)|*.csv";
            //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();  //tanya anak dokter dong, ini buat apa dibagian nullablenya


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                TbDirectory.Text = filename;
            }

        }

        /// <summary>
        /// Dari input, masuk ke clipboard
        /// </summary>
        /// <param name="input"></param>
        public void SetTextToClipboard(string input)
        {
            try
            {
                Clipboard.SetText(input);
            }
            catch (ArgumentNullException)
            {
                Clipboard.SetText(" ");
            }

        }


        /// <summary>
        /// lempar variable input ke box "Chapter Name"
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="input"></param>
        public void SendToChapterNameBox(IWebDriver driver, string input)
        {
            //Send array to "Chapter Name Box"

            //OpenQA.Selenium.ElementNotVisibleException
            try
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).Clear();

                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ElementNotVisibleException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (NoSuchElementException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(input);
            }
        }

        /// <summary>
        /// Klik di bagian Amazon Search album
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="input"></param>
        public void AmazonSearchAlbum(IWebDriver driver, string input)
        {
            //Send array to "Search by Album Name"
            //driver.FindElement(By.Name("blogspot-album-name")).SendKeys(sl);
            try
            {
                driver.FindElement(By.Name("amazon-folder-url")).Clear();
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ArgumentNullException)
            {
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(input);
            }



        }


        /// <summary>
        /// masih kosong bung , saya lupa
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="input"></param>
        public void WpCreateChapter(IWebDriver driver, string input)
        {


            driver.FindElement(By.Id("import-album")).Click();
            string loadicon = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");



            while (loadicon.Contains("block"))
            {
                Console.WriteLine(loadicon);
                System.Threading.Thread.Sleep(100);
                loadicon = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");

            }



            //ini bisa
            /*
            try
            {
                string loadicon2 = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");
                Console.WriteLine(loadicon2 + "test 2");
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("2 failed");
            }*/


            /* ini bisa juga
            try
            {

                string loadicon3 = driver.FindElement(By.XPath("//div[@class='wp-manga-popup-loading']")).GetAttribute("style");
                Console.WriteLine(loadicon3 +"test 3");
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("3 failed");
            }*/


            /*OpenQA.Selenium.WebDriverException: 'unknown error: Element <button type="button" class="button button-primary" id="import-album" title="Search Album">...</button> is not clickable at point (267, 551). Other element would receive the click: <div class="wp-manga-popup-loading" style="display: block;">...</div>
  (Session info: chrome=71.0.3578.98)
  (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 10.0.10586 x86_64)'
*/

            //kalo texbox yg ijo ato merah muncul... masukin ke stringlist(eror or complete)
            //           if (driver.FindElement(By.Id("chapter-upload-msg")).Displayed && driver.FindElement(By.Id("chapter-upload-msg")).Text == "Invalid AmazonS3 Folder URL")

            /*
            if (driver.FindElement(By.Id("chapter-upload-msg")).Text == "Invalid AmazonS3 Folder URL")
            {
                //Invalid URL
                //MessageBox.Show("cannot find this album true");
                errorlist.Add(input);
                Console.WriteLine("error = " + input);
            }
            else
            {
                //click on create chapter

                //add to stringlist
                completelist.Add(input);
                Console.WriteLine("complete = " + input);
            }*/

        }

        public void PublishClick(IWebDriver driver)
        {
           
            try
            {
                driver.FindElement(By.XPath("//div[@id='publishing-action']/input[2]")).Click();

            }

            catch(OpenQA.Selenium.WebDriverException)
            {
                Console.WriteLine("fail 1");
                try
                {
                    driver.FindElement(By.CssSelector("#publish")).Click();
                }
                catch (OpenQA.Selenium.WebDriverException)
                {
                    Console.WriteLine("fail2");

                    driver.FindElement(By.XPath("//input[@id='publish']")).Click();

                }
            }






        }

        /// <summary>
        /// Tombol Submit Chapter Uploader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtGO_Click(object sender, RoutedEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //variabel mangadir dan link
            //List<string> listA = new List<string>();    //listA = mangadirectory
            //List<string> listB = new List<string>();    //listB = manga link
            var list = new List<Tuple<string, string, string>>();


            //membaca file csv
            using (var reader = new StreamReader(TbDirectory.Text))
            {

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    //listA.Add(values[0]);
                    //listB.Add(values[1]);
                    list.Add(new Tuple<string, string, string>(values[0], values[1], values[2]));

                }
            }

            //https://stackoverflow.com/questions/1955766/iterate-two-lists-or-arrays-with-one-foreach-statement-in-c-sharp

            //melakukan looping sejumlah dengan banyak record yang ada di csv
            foreach (var manga in list)
            {
                //untuk first time login saja
                Boolean looponce = true;

                FetchFromDirectory(manga.Item1);


                //added options
                ChromeOptions option = new ChromeOptions();
                //option.AddArgument("--headless");
                option.AddArgument("--blink-settings=imagesEnabled=false");

                //open chrome
                IWebDriver driver = new ChromeDriver(option);
                
                LoginToWordPress(driver);

                //ganti isi textbox manganame sesuai dengan yang ada di csv
                TBMangaName.Text = manga.Item3;

                System.Threading.Thread.Sleep(2000);


                //melakukan looping per mangafolder yang sudah di lempar ke var sourcelist
                foreach (var sl in sourcelist)
                {

                    ///sebelum amazondetailedurl, harus dirubah dulu untuk folder wasabi nya(TBMangaName.text) di looping yang pas csv (diluar loop sl ini)
                    string amazondetailedurl = TbAmazonURL.Text + TBBucket.Text + "/" + TBMangaName.Text + "/" + sl + TBRegion.Text;
                    string temp = sl.Replace("_", " ").Replace("~", ".");

                    if (looponce == true)
                    {
                        GoToMangaLink(driver,manga.Item2);
                        ClickUploadSingleChapter(driver);
                    }

                    SetTextToClipboard(temp);
                    SendToChapterNameBox(driver, temp);

                    if (looponce == true)
                    {
                        driver.FindElement(By.Id("select-album")).Click();
                    }

                    SetTextToClipboard(amazondetailedurl);
                    AmazonSearchAlbum(driver, amazondetailedurl);
                    WpCreateChapter(driver, sl);
                    looponce = false;


                }
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("scrollTo(0,0)"); //go to top
                PublishClick(driver);
                driver.Quit();
                sw.Stop();
                Console.WriteLine("Elapsed={0}", sw.Elapsed);



                //NOTES!!!! untuk csv... col1 = mangadir, col2= mangalink, col3 = manganame yang ada di wasabi

            }
        }
    }
}
