﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;

namespace XUploaderReNew.User_Control
{
    /// <summary>
    /// Interaction logic for UC_ChapterUploader.xaml
    /// </summary>
    public partial class UC_ChapterUploader : UserControl
    {

        List<String> sourcelist = new List<String>(); //list manga url
        List<String> errorlist = new List<String>();
        List<String> completelist = new List<String>();


        //public string from amazonform
        string amazonurl, amazonbucket, amazonmanganame, amazonregion;

        public UC_ChapterUploader()
        {
            InitializeComponent();
        }

        private void BtFetchFromClipboard_Click(object sender, RoutedEventArgs e)
        {
            //kosongin
            sourcelist = new List<string>();

            try
            {
                //split dari clipboard
                var gettext = System.Windows.Forms.Clipboard.GetText().Split('\n').ToList();


                //
                foreach (var line in gettext)
                {
                    sourcelist.Add(line);
                }

                MessageBox.Show("copied from clipboard");

                foreach(string s in sourcelist)
                {
                    Console.WriteLine(s);
                }

            }
            catch (System.ArgumentNullException)
            {
                MessageBox.Show("clipboard is null");
            }
            
        }

        private void BtFetchFromDirectory_Click(object sender, RoutedEventArgs e)
        {

            //kosongin
            sourcelist = new List<string>();


            //path
            string path = TbDirectory.Text;
            int temp = 0;

            Console.WriteLine(path);

            DirectoryInfo dir = new DirectoryInfo(path);



            foreach(string s in Directory.GetDirectories(path))
            {
                sourcelist.Add((s.Remove(0, path.Length + 1)));
            }

            foreach(string s in sourcelist)
            {
                Console.WriteLine(s);
                temp++;
            }

            MessageBox.Show(temp + " lines fetched\nCopied From Directory");

        }

        private void BtSourceList_Click(object sender, RoutedEventArgs e)
        {


            string temp = "";

            foreach (string s in sourcelist)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }

            MessageBox.Show("Source List Copied to Clipboard");
        }



        public void LoginToWordPress(IWebDriver driver)
        {
            //go to wp url
            driver.Url = Properties.Settings.Default.WpLink;

            //fill username to cell
            driver.FindElement(By.Id("user_login")).SendKeys(Properties.Settings.Default.WpUsername);

            //fill password to cell
            driver.FindElement(By.Id("user_pass")).SendKeys(Properties.Settings.Default.WpPassword);

            //click login
            driver.FindElement(By.Id("wp-submit")).Click();


        }

        public void GoToMangaLink(IWebDriver driver)
        {
            try
            {
                driver.Url = TbWpMangaLink.Text;
                //uncomment pls
                //System.Threading.Thread.Sleep(delay);
            }
            catch (WebDriverTimeoutException)
            {


                try
                {

                    driver.Url = TbWpMangaLink.Text;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
                catch (WebDriverException)
                {

                    driver.Url = TbWpMangaLink.Text;
                    //uncomment pls
                    //System.Threading.Thread.Sleep(delay);
                }
            }

        }


        public void ClickUploadSingleChapter(IWebDriver driver)
        {
            //click "Upload Single Chapter" 

            //kykny perlu klik di bagian a href nya
            //disini ada masalah webdriverexception tadinya

            try
            {

                driver.FindElement(By.ClassName("manga-tab-select tab-active")).Click();
            }

            catch (NoSuchElementException)
            {
                try
                {
                    driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[2]/div/div[1]/div[1]/ul/li[3]/a")).Click();


                }

                catch (NoSuchElementException)
                {

                    try
                    {
                        driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[5]/form/div[2]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/ul/li[3]/a")).Click();

                    }
                    catch (NoSuchElementException)
                    {
                        driver.FindElement(By.ClassName("manga-tab-select")).Click();

                    }

                }

            }
        }


        public void SetTextToClipboard(string input)
        {
            try
            {
                Clipboard.SetText(input);
            }
            catch (ArgumentNullException)
            {
                Clipboard.SetText(" ");
            }

        }


        public void SendToChapterNameBox(IWebDriver driver, string input)
        {
            //Send array to "Chapter Name Box"

            //OpenQA.Selenium.ElementNotVisibleException
            try
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).Clear();

                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ElementNotVisibleException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (NoSuchElementException)
            {
                driver.FindElement(By.Name("wp-manga-chapter-name")).SendKeys(input);
            }
        }

        private void RbAmazon_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ChapterUploader.AmazonSourceForm form = new ChapterUploader.AmazonSourceForm();
            form.ShowDialog();

            amazonurl = form.TbAmazonURL.Text;
            amazonbucket = form.TBBucket.Text;
            amazonmanganame = form.TBMangaName.Text;
            amazonregion = form.TBRegion.Text;
        }

        private void BtErrorList_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";

            foreach (string s in errorlist)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }

            MessageBox.Show("Error List Copied to Clipboard");

        }

        private void BtCompleteList_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";

            foreach (string s in completelist)
            {
                if (temp == "")
                {
                    temp = s;
                }
                else
                {
                    temp = temp + '\n' + s;
                }



            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }

            MessageBox.Show("Complete List Copied to Clipboard");



        }

        public void AmazonSearchAlbum(IWebDriver driver, string input)
        {
            //Send array to "Search by Album Name"
            //driver.FindElement(By.Name("blogspot-album-name")).SendKeys(sl);
            try
            {
                driver.FindElement(By.Name("amazon-folder-url")).Clear();
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (ArgumentNullException)
            {
                driver.FindElement(By.Name("amazon-folder-url")).SendKeys(input);
            }



        }

        public void WpCreateChapter(IWebDriver driver, string input)
        {
            

            driver.FindElement(By.Id("import-album")).Click();
            string loadicon = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");

   

            while (loadicon.Contains("block"))
            {
                Console.WriteLine(loadicon);
                System.Threading.Thread.Sleep(100);
                loadicon = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");

            }



            //ini bisa
            /*
            try
            {
                string loadicon2 = driver.FindElement(By.ClassName("wp-manga-popup-loading")).GetAttribute("style");
                Console.WriteLine(loadicon2 + "test 2");
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("2 failed");
            }*/


            /* ini bisa juga
            try
            {

                string loadicon3 = driver.FindElement(By.XPath("//div[@class='wp-manga-popup-loading']")).GetAttribute("style");
                Console.WriteLine(loadicon3 +"test 3");
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("3 failed");
            }*/


            /*OpenQA.Selenium.WebDriverException: 'unknown error: Element <button type="button" class="button button-primary" id="import-album" title="Search Album">...</button> is not clickable at point (267, 551). Other element would receive the click: <div class="wp-manga-popup-loading" style="display: block;">...</div>
  (Session info: chrome=71.0.3578.98)
  (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 10.0.10586 x86_64)'
*/

            //kalo texbox yg ijo ato merah muncul... masukin ke stringlist(eror or complete)
            //           if (driver.FindElement(By.Id("chapter-upload-msg")).Displayed && driver.FindElement(By.Id("chapter-upload-msg")).Text == "Invalid AmazonS3 Folder URL")

            if (driver.FindElement(By.Id("chapter-upload-msg")).Text == "Invalid AmazonS3 Folder URL")    
            {
                //Invalid URL
                //MessageBox.Show("cannot find this album true");
                errorlist.Add(input);
                Console.WriteLine("error = " + input);
            }
            else
            {
                //click on create chapter
                
                //add to stringlist
                completelist.Add(input);
                Console.WriteLine("complete = " + input);
            }

        }

        public void BtRunChapterUploader_Click(object sender, RoutedEventArgs e)
        {
            Boolean looponce = true; //untuk loop sekali

            //open chrome
            IWebDriver driver = new ChromeDriver();


            //System.Threading.Thread.Sleep(2000);


            LoginToWordPress(driver);

            //IMPORTANT MESSAGE

            /*
             * utk url masuknya sekali aja , biar single upload chapternya ga ke klik yg lain. 
             * loopnya cuma di typing chapter name ama klik upload aja 
             * kalo messagebox ditulis 'Created Complete!' itu masuk complete list
             * tapi utk error list ada masalah dikit
             * pertama dia keluar string 'Invalid URL'
             * kedua Invalid AmazonS3 Folder URL
             * 
             * */

            //looping per chapter line
            foreach (var sl in sourcelist)
            {

                string amazondetailedurl = amazonurl + amazonbucket + "/" + amazonmanganame + "/" + sl + amazonregion;
                //string replace = amazondetailedurl.Replace("\n", string.Empty);


                //MessageBox.Show(replace);


                //always change to manga the same manga link from textbox
                if(looponce == true)
                {

                    GoToMangaLink(driver);
                    //click upload single chapter
                    ClickUploadSingleChapter(driver);
                }
                

                

                //modified string "_" become " "
                string temp = sl.Replace("_", " ").Replace("~", ".");
                //Console.WriteLine(temp);
                //temp = sl.Replace("~", ".");
                //Console.WriteLine(temp);
                //temp = sl.Replace(",", ".");

                //set chapter title to clipboard
                SetTextToClipboard(temp);

                //Console.WriteLine(temp);

                System.Threading.Thread.Sleep(500);

                //paste chapter name to box
                SendToChapterNameBox(driver, temp);

                //Click "Select Uploaded Album From Cloud Server" Radio Button

                if(looponce == true)
                {
                    driver.FindElement(By.Id("select-album")).Click();
                }

                 //error before? ini klik select uploaded album from cloud server, nanti di loop sekali aja biar ga error
                /*
                 * OpenQA.Selenium.WebDriverException: 'unknown error: Element <input type="radio" name="wp-manga-upload-type" value="select-album" id="select-album"> is not clickable at point (354, 285). Other element would receive the click: <div class="wp-manga-popup-loading" style="display: block;">...</div>
  (Session info: chrome=71.0.3578.98)
  (Driver info: chromedriver=2.45.615291 (ec3682e3c9061c10f26ea9e5cdcf3c53f3f74387),platform=Windows NT 10.0.10586 x86_64)'

                 * */


                //Console.WriteLine(amazondetailedurl);
                //set clipboard to amazon bucket link
                SetTextToClipboard(amazondetailedurl);
                Console.WriteLine(amazondetailedurl);

                //search amazon album
                AmazonSearchAlbum(driver, amazondetailedurl);


                //click create chapter
                WpCreateChapter(driver, sl);

                System.Threading.Thread.Sleep(1000);
                looponce = false;

            }


            //from amazon
            if (RbAmazon.IsChecked == true)
            {



            }
        }
    }
}
