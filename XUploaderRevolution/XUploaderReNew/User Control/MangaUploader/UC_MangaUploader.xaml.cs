﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;

namespace XUploaderReNew.User_Control
{
    /// <summary>
    /// Interaction logic for UC_MangaUploader.xaml
    /// </summary>
    public partial class UC_MangaUploader : UserControl
    {

        List<String> mangalist = new List<string>(); //manga source url
        //string wpurl = "";
        string mangatitle = "";
        string mangadescription = "";
        string mangaauthor = "";
        string mangaartist = "";
        string mangayear = "";
        string mangacategory = "";
        string mangatype = "";
        //string mangayear = "";



        public UC_MangaUploader()
        {
            InitializeComponent();
        }

        private void BtFetchFromClipboard_Click(object sender, RoutedEventArgs e)
        {
            var lines = System.Windows.Forms.Clipboard.GetText().Split('\n').ToList();

            mangalist = new List<string>(); //zeroing manga list

            foreach (string s in lines)
            {
                mangalist.Add(s);

            }

            MessageBox.Show("Copied From Clipboard");
        }

        private void BtCheckToClipboard_Click(object sender, RoutedEventArgs e)
        {


            string temp = "";
            int dumb = 1;

            foreach (string s in mangalist)
            {
                if (temp == "")
                {
                    temp = "line " + dumb + " = " + s;
                    //dumb++;
                }
                else
                {
                    //temp = "line " + dumb + " = " + s + '\n' + temp;
                    temp = temp + "line " + dumb + " = " + s + '\n' ;
                }
                Console.WriteLine(s);
                dumb++;


            }

            try
            {
                System.Windows.Forms.Clipboard.SetText(temp);
            }
            catch (System.ArgumentNullException)
            {

            }

            MessageBox.Show("Written To Clipboard, please check");


        }

        private void BtSubmit_Click(object sender, RoutedEventArgs e)
        {
            string username = Properties.Settings.Default.WpUsername;
            string password = Properties.Settings.Default.WpPassword;

            
            


            
            foreach(string s in mangalist)
            {

                //open chrome for url
                IWebDriver driver = new ChromeDriver();

                //loop manga url per lines
                driver.Url = s;




                //test
                //test123

                //for mangadex
                if (RbMangadex.IsChecked == true)
                {
                    //mangatitle = driver.FindElement(By.XPath("//meta[@property='WHATIS THIS']")).GetAttribute("content");

                    //get the description
                    //https://stackoverflow.com/questions/25973997/retrieve-the-content-of-meta-description-from-web-page-using-selenium-webdriver
                    //mangadescription = driver.FindElement(By.CssSelector("meta[property='description']")).GetAttribute("content");
                    mangadescription = driver.FindElement(By.XPath("//meta[@property='og:description']")).GetAttribute("content");

                    //get manga title
                    mangatitle = driver.FindElement(By.XPath("/html/body/div[1]/div[2]/h6")).Text;

                    //mangatitle = driver.FindElement(By.XPath("//meta[@property='og:title']")).GetAttribute("content");

                    //get manga author
                    mangaauthor = driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/a")).Text;

                    //get manga artist
                    mangaartist = driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div/div[2]/div[3]/div[2]/a")).Text;


                }
                else if(RbMangaUpdate.IsChecked == true)
                {
                    //mangatitle = driver.FindElement(By.XPath("//td[@id='main_content']/table[2]/tbody/tr/td/div/div/span")).Text;
                    //mangatitle = driver.FindElement(By.CssSelector("div[id^='mu-main-content'] span."))

                    //gua ada update disini
                    //disini juga
                    mangatitle = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[1]/span[1]")).Text;

                    if (IsElementPresent(driver,By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[3]/div[2]/div[1]/a/u")))
                    {
                        driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[3]/div[2]/div[1]/a/u")).Click();
                        mangadescription = driver.FindElement(By.CssSelector("div.sContent")).Text;

                        mangadescription = mangadescription.Replace(" Less...","");
                        mangadescription = mangadescription.Replace("Less...", "");
                        mangadescription = mangadescription.Replace("Less. . .", "");
                        mangadescription = mangadescription.Replace(" Less... ", "");
                        //MessageBox.Show(mangadescription);
                        //another test
                    }
                    else
                    {
                        mangadescription = driver.FindElement(By.CssSelector("div.sContent")).Text;


                    }
                    //sama disini
                    mangadescription = driver.FindElement(By.CssSelector("div.sContent")).Text;
                    Console.WriteLine(mangadescription);

                    mangaauthor = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[4]/div[12]/a/u")).Text;


                    mangaartist = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[4]/div[14]/a/u")).Text;

                    try
                    {
                        mangacategory = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[4]/div[8]/a[1]/u")).Text;

                    }
                    catch (NoSuchElementException)
                    {
                        mangacategory = "";

                    }

                    mangatype = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[3]/div[4]")).Text;

                    mangayear = driver.FindElement(By.XPath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[1]/div[4]/div[16]")).Text;
                    Console.WriteLine(mangayear + " " + mangatype + " " + mangacategory);

                }



                //goto wp
                //driver.Navigate().GoToUrl(Properties.Settings.Default.WpLink);
                driver.Url = Properties.Settings.Default.WpLink;

                //fill username
                WpSetUsername(driver, username);

                //fill password
                WpSetPassword(driver, password);

                //click login
                driver.FindElement(By.Id("wp-submit")).Click();

                //insert new manga
                driver.Url = "https://mangaflame.com/wp-admin/post-new.php?post_type=wp-manga";

                WpSetMangaTitle(driver, mangatitle);


                //tulis deskripsi di editor wordpress
                driver.SwitchTo().Frame("content_ifr");
                driver.FindElement(By.Id("tinymce")).Click();

                //insert description
                WpSetMangaDescription(driver, mangadescription);

                //go to  parent frame
                driver.SwitchTo().ParentFrame();

                //insert manga SEO title (NOT YOAST)
                WpSetMangaSEOTitle(driver, mangatitle);

                //insert manga SEO description (NOT YOAST)
                WpSetMangaSEODescription(driver, mangadescription);


                //CLICK THE YOAST SEO
                IWebElement myelement = driver.FindElement(By.XPath("//div[@id='wpseo-metabox-root']/div/div/div/section/div/button"));
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                js.ExecuteScript("arguments[0].scrollIntoView()", myelement);
                //js.ExecuteScript("scrollBy(0,-30)");
                js.ExecuteScript("scrollBy(0, -100)");
                driver.FindElement(By.CssSelector(".ggHoPn")).Click();

                //insert manga SEO Description (YOAST)
                WPSetMangaSEOYoastDescription(driver, mangadescription);


                //insert manga SEO focus keyword(YOAST)
                WpSetMangaSEOYoastFocusKeyword(driver, mangatitle);


                //klik manga type
                //ERROR KETIKA BEDA USERNAME(ADMIN BARU)
                //ganti di widget biar abis text editor baru yoast baru manga type (soalnya default text editor, manga type baru yoast makanya error)
                WpSetMangaTypeOptions(driver);


                //set manga alternative name
                WpSetMangaAlternativeName(driver, mangatitle);



                //set manga artist
                WpSetMangaArtist(driver, mangaartist);


                //set manga author
                WpSetMangaAuthor(driver, mangaauthor);

                //button tagadd author
                //yg ini error?
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/div/div/div[3]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[4]/div/div/div/div/div[2]/input[2]")).Click();
                //driver.FindElement(By.CssSelector("#wp-manga-author .button")).Click();


                //button tagadd artist
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/div/div/div[3]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[5]/div/div/div/div/div[2]/input[2]")).Click();
                //driver.FindElement(By.CssSelector("css=#wp-manga-artist .button")).Click();

                WpSetMangaType(driver, mangatype);

                WpSetMangaYear(driver, mangayear);
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/div/div/div[3]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[3]/div/div/div/div/div[2]/input[2]")).Click();



                //set manga tag
                WpSetMangaTag(driver, mangatitle);



                

                //click add manga tag button
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/div/div/div[3]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[7]/div/div/div/div/div[2]/input[2]")).Click();
                // /html/body/div[1]/div[2]/div[2]/div[1]/div[4]/form/div/div/div[3]/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[7]/div/div/div/div/div[2]/p/input[2]


                //driver.FindElement(By.ClassName("save")).Click();
                //driver.FindElement(By.CssSelector("#save-post")).Click();

                //scroll to top to save draft
                //window.scrollTo(300, 500);
                // js.ExecuteScript("window.scrolTo(0,0)");
                js.ExecuteScript("scrollTo(0,0)");
                //Post draft updated. 
                System.Threading.Thread.Sleep(2500);    //AVISCERA2
                driver.FindElement(By.XPath("//input[@id='save-post']")).Click();



                //quit
                driver.Quit();

                //clear clipboard
                Clipboard.SetText(" ");

            }


        }

        public void WpSetUsername(IWebDriver driver, string username)
        {
            //fill username to cell
            try
            {
                Clipboard.SetText(username);
                driver.FindElement(By.Id("user_login")).SendKeys(OpenQA.Selenium.Keys.Control + "v");

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("user_login")).SendKeys(username);

            }
        }

        public void WpSetPassword(IWebDriver driver, string password)
        {
            try
            {
                Clipboard.SetText(password);
                driver.FindElement(By.Id("user_pass")).SendKeys(OpenQA.Selenium.Keys.Control + "v");


            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("user_pass")).SendKeys(password);

            }
        }


        public void WpSetMangaTitle(IWebDriver driver, string mangatitle)
        {
            try
            {
                Clipboard.SetText(mangatitle);
                driver.FindElement(By.Id("title")).SendKeys(OpenQA.Selenium.Keys.Control + "v");

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("title")).SendKeys(mangatitle);

            }

        }


        public void WpSetMangaDescription(IWebDriver driver, string mangadescription)
        {
            try
            {
                Clipboard.SetText(mangadescription);
                driver.FindElement(By.Id("tinymce")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("tinymce")).SendKeys(mangadescription);
            }

        }

        public void WpSetMangaSEOTitle(IWebDriver driver, string mangatitle)
        {
            try
            {
                Clipboard.SetText(mangatitle);
                driver.FindElement(By.Id("manga_meta_title")).SendKeys(OpenQA.Selenium.Keys.Control + "v");

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("manga_meta_title")).SendKeys(mangatitle);
            }
        }

        public void WpSetMangaSEODescription(IWebDriver driver, string mangadescription)
        {
            try
            {
                Clipboard.SetText(mangadescription);
                driver.FindElement(By.Id("manga_meta_desc")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("manga_meta_desc")).SendKeys(mangadescription);
            }
        }

        public void WPSetMangaSEOYoastDescription(IWebDriver driver, string mangadescription)
        {
            try
            {
                Clipboard.SetText(mangadescription);

                driver.FindElement(By.Id("snippet-editor-field-description")).Click();
                driver.FindElement(By.Id("snippet-editor-field-description")).SendKeys(OpenQA.Selenium.Keys.End);
                //paste data clipboard
                driver.FindElement(By.Id("snippet-editor-field-description")).SendKeys((OpenQA.Selenium.Keys.Control + "v") + " ");
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("snippet-editor-field-description")).SendKeys(mangadescription + " ");
            }
        }

        public void WpSetMangaSEOYoastFocusKeyword(IWebDriver driver,string mangatitle)
        {
            try
            {
                try
                {
                    Clipboard.SetText(mangatitle);
                    driver.FindElement(By.Id("focus-keyword-input")).SendKeys(OpenQA.Selenium.Keys.Control + "v");

                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    driver.FindElement(By.Id("focus-keyword-input")).SendKeys(mangatitle);

                }
            }
            catch (NoSuchElementException)
            {
                driver.FindElement(By.Id("yoast-seo-analysis-collapsible-metabox")).Click();

                try
                {
                    Clipboard.SetText(mangatitle);
                    driver.FindElement(By.Id("focus-keyword-input")).SendKeys(OpenQA.Selenium.Keys.Control + "v");

                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    driver.FindElement(By.Id("focus-keyword-input")).SendKeys(mangatitle);

                }
            }
        }

        public void WpSetMangaTypeOptions(IWebDriver driver)
        {
            try
            {
                driver.FindElement(By.CssSelector("div.manga-type-choice > #wp-manga-type")).Click();

            }
            catch (ElementNotInteractableException)
            {
                try
                {
                    driver.FindElement(By.CssSelector(".manga-type-choice > #wp-manga-type")).Click();
                }
                catch (ElementNotInteractableException)
                {
                    try
                    {
                        driver.FindElement(By.XPath("(//input[@id='wp-manga-type'])[2]")).Click();
                    }
                    catch (ElementNotInteractableException)
                    {
                        try
                        {
                            driver.FindElement(By.XPath("//div[@id='manga-information-metabox']/div/div/div[3]/div/div[2]/input")).Click();
                        }
                        catch (ElementNotInteractableException)
                        {
                            Console.WriteLine("rip");
                        }
                    }
                }
            }
        }

        private bool IsElementPresent(IWebDriver driver,By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        

        public void WpSetMangaAlternativeName(IWebDriver driver, string mangatitle)
        {
            try
            {
                Clipboard.SetText(mangatitle);
                driver.FindElement(By.Id("wp-manga-alternative")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));
            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("wp-manga-alternative")).SendKeys(mangatitle);

            }
        }


        public void WpSetMangaYear(IWebDriver driver, string mangayear)
        {
            try
            {
                Clipboard.SetText(mangayear);
                driver.FindElement(By.Id("new-tag-wp-manga-release")).SendKeys(OpenQA.Selenium.Keys.Control + "v");
            }
            catch (System.ArgumentNullException)
            {

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("new-tag-wp-manga-release")).SendKeys(mangayear);
            }

        }



        public void WpSetMangaArtist(IWebDriver driver, string mangaartist)
        {
            try
            {
                Clipboard.SetText(mangaartist);
                driver.FindElement(By.Id("new-tag-wp-manga-artist")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));
            }
            catch (System.ArgumentNullException)
            {

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("new-tag-wp-manga-artist")).SendKeys(mangaartist);
            }
        }

        public void WpSetMangaAuthor(IWebDriver driver, string mangaauthor)
        {
            try
            {
                Clipboard.SetText(mangaauthor);
                driver.FindElement(By.Id("new-tag-wp-manga-author")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));

            }
            catch (System.ArgumentNullException)
            {

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("new-tag-wp-manga-author")).SendKeys(mangaauthor);

            }
        }

        public void WpSetMangaType(IWebDriver driver, string mangatype)
        {
            try
            {
                Clipboard.SetText(mangatype);
                driver.FindElement(By.Id("wp-manga-type")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));
            }
            catch (System.ArgumentNullException)
            {

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("wp-manga-type")).SendKeys(mangatype);

            }


        }


        public void WpSetMangaTag(IWebDriver driver, string mangatitle)
        {
            try
            {
                Clipboard.SetText(mangatitle);

                driver.FindElement(By.Id("new-tag-wp-manga-tag")).SendKeys((OpenQA.Selenium.Keys.Control + "v"));

            }
            catch (System.Runtime.InteropServices.ExternalException)
            {
                driver.FindElement(By.Id("new-tag-wp-manga-tag")).SendKeys(mangatitle);


            }
        }

    }
}
