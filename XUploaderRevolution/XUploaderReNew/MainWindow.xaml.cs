﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace XUploaderReNew
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        
        



        private void BtPopUpExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void BtPopUpSettings_Click(object sender, RoutedEventArgs e)
        {
            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new User_Control.UC_Login());
        }

        private void BtMangaUploader_Click(object sender, MouseButtonEventArgs e)
        {

            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new User_Control.UC_MangaUploader());
            
        }

        private void BtChapterUploader_Click(object sender, MouseButtonEventArgs e)
        {
            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new User_Control.UC_ChapterUploader());
        }

        private void BtFetchListMangadex_Click(object sender, MouseButtonEventArgs e)
        {
            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new User_Control.UC_FetchListMangadex());
            
        }
        

        private void BtPopUpMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void WindowDrag_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void HeaderGrab_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void BtFetchRssFeed_Click(object sender, MouseButtonEventArgs e)
        {
            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new User_Control.FetchRssFeed.FetchRssFeed());
        }

        private void BtMultiChapterUploader_Click(object sender, MouseButtonEventArgs e)
        {
            MainWindowPage.Children.Clear();

            MainWindowPage.Children.Add(new XUploaderReNew.User_Control.ChapterUploader.UC_MultiChapterUploader());

        }

        private void BtScrapeToDatabase(object sender, MouseButtonEventArgs e)
        {
            MainWindowPage.Children.Clear();
            MainWindowPage.Children.Add(new XUploaderReNew.ScrapeToDatabase());
        }
    }
}
